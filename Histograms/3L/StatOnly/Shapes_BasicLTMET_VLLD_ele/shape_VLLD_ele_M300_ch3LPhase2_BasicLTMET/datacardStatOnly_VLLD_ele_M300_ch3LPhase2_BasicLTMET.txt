imax * number of channels
jmax * number of backgrounds
kmax * number of nuisance parameters (sources of systematical uncertainties)
shapes * * SHAPES_VLLD_ele_M300_ch3LPhase2_BasicLTMET.root $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC


bin          ch3LPhase2_BasicLTMET 
observation  301826.08695652167     

bin      ch3LPhase2_BasicLTMET  ch3LPhase2_BasicLTMET  
process  VLLD_ele_M300   bkg   
process  0     1     
rate     35301.216322525026 301765.1826086956 
----------------------------------------------------------------------------------------------


* autoMCStats 0 0 1

