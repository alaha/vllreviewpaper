imax * number of channels
jmax * number of backgrounds
kmax * number of nuisance parameters (sources of systematical uncertainties)
shapes * * SHAPES_VLLD_ele_M1600_ch3LPhase2_BasicLTMET.root $CHANNEL_$PROCESS $CHANNEL_$PROCESS_$SYSTEMATIC


bin          ch3LPhase2_BasicLTMET 
observation  301826.08695652167     

bin      ch3LPhase2_BasicLTMET  ch3LPhase2_BasicLTMET  
process  VLLD_ele_M1600   bkg   
process  0     1     
rate     9.110528405537828 301765.1826086956 
----------------------------------------------------------------------------------------------
lumi lnN 1.01 1.01 
leptonid lnN 1.015 1.015 
Syst20 lnN 1.2 1.2 

* autoMCStats 0 0 1

